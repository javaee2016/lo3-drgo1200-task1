package se.miun.dsv.jee.jpa.example;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import se.miun.dsv.jee.jpa.example.model.*;


public class DAO {

	private static EntityManagerFactory entityManagerFactory = Persistence
			.createEntityManagerFactory("example");

	public void persistAll(Set<User> users) {
		EntityManager manager = entityManagerFactory.createEntityManager();

		EntityTransaction tx = manager.getTransaction();
		try {

			tx.begin();

			for (User user : users)
				manager.persist(user);

			tx.commit();

		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public void persistEvent(Event event) {
		EntityManager manager = entityManagerFactory.createEntityManager();

		EntityTransaction tx = manager.getTransaction();
		try {

			tx.begin();

			manager.merge(event);

			tx.commit();

		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Event> queryEventsByCity(String city) {
		EntityManager manager = entityManagerFactory.createEntityManager();
		List<Event> events;
		events = (List<Event>) manager.createQuery("SELECT e FROM Event e WHERE lower(e.city) LIKE :cityName")
				.setParameter("cityName", (city + "%").toLowerCase()).getResultList();
		manager.close();
		return events;
	}
	
	@SuppressWarnings("unchecked")
	public List<Event> queryAllEvents() {
		EntityManager manager = entityManagerFactory.createEntityManager();
		List<Event> events;
		events = (List<Event>) manager.createQuery("SELECT e FROM Event e ORDER BY e.startTime").getResultList();
		manager.close();
		return events;
	}
	

	public Event queryEventById(long tmpId) {
		EntityManager manager = entityManagerFactory.createEntityManager();
		Event event;
		event = (Event) manager.createQuery("SELECT e FROM Event e WHERE e.id = :id")
				.setParameter("id", tmpId).getSingleResult();
		manager.close();
		return event;
	}
	
	public User queryUserById(long tmpId) {
		EntityManager manager = entityManagerFactory.createEntityManager();
		User user;
		user = (User) manager.createQuery("SELECT e FROM User e WHERE e.id = :id")
				.setParameter("id", tmpId).getSingleResult();
		manager.close();
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<User> queryAllUsers() {
		EntityManager manager = entityManagerFactory.createEntityManager();
		List<User> users;
		users = (List<User>) manager.createQuery("SELECT e FROM User e").getResultList();
		manager.close();
		return users;
	}

	public void shutDown() {
		entityManagerFactory.close();
	}
}
