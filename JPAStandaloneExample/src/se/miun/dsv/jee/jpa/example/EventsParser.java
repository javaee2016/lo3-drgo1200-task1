package se.miun.dsv.jee.jpa.example;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;



import se.miun.dsv.jee.jpa.example.model.Comment;
import se.miun.dsv.jee.jpa.example.model.Event;
import se.miun.dsv.jee.jpa.example.model.Organizer;
import se.miun.dsv.jee.jpa.example.model.User;

public class EventsParser {

	private Set<User> users = new HashSet<User>();
	private Set<Event> events = new HashSet<Event>();
	private Set<Comment> comments = new HashSet<Comment>();
	private Set<Organizer> organizers = new HashSet<Organizer>();
	boolean readUsers,readEvents,readComment;
	DateTimeFormatter dateTimeFormatter;
	LocalDateTime start,stop;
	String eventStringId;

	URL url;
	Scanner s;

	public EventsParser() {

		readUsers = false;
		readEvents=false;
		dateTimeFormatter = DateTimeFormatter.ofPattern("yy/MM/dd HH:mm");

		try {
			this.url = new URL("https://bitbucket.org/javaee2016/javaee16/raw/master/material/events.txt");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



	}

	public void parse() {

		try {
			s = new Scanner(this.url.openStream(), "UTF-8");

			while(s.hasNextLine())
			{
				String tmp = s.nextLine();

				if(tmp.equals("Users:"))
				{
					readUsers = true;
					//System.out.println("USERS");
					tmp = s.nextLine();

				}

				if(tmp.equals("Events:"))
				{
					readUsers = false;
					readEvents=true;
					//System.out.println("EVENTS");
					tmp = s.nextLine();

				}

				if(readUsers)
				{
					if(!tmp.equals(""))
					{
						String[] tokens = tmp.split("\\s");
						if(tokens.length == 3)
						{
							User user = new User(tokens[0].toString(),tokens[1].toString(),tokens[2].toString());
							users.add(user);
							//System.out.println("-" + user.getFirstName() + "-" + user.getLastName() + "-"  + user.getEmail() + "-");
						}

					}
				}

				if(readEvents)
				{
					if(!tmp.equals(""))
					{
						if(Character.isWhitespace(tmp.charAt(0)))
						{
							tmp = tmp.replaceFirst("\\s", "");
							String[] tokens = tmp.split("[()]");
							//System.out.println("Comment: " + tmp);
							for(int tokPos =0; tokPos < tokens.length; tokPos++)
							{
								if(tokens[tokPos].startsWith(":"))
								{
									tokens[tokPos] = tokens[tokPos].replaceFirst("\\:", "");
								}
								if(tokens[tokPos].startsWith(" "))
								{
									tokens[tokPos] = tokens[tokPos].replaceFirst("\\s", "");
								}


								tokens[tokPos] = tokens[tokPos].replaceAll("\"","");
								tokens[tokPos] = tokens[tokPos].trim();
								//System.out.println(tokens[tokPos]);
							}
							//System.out.println(tokens[0].toString() + " " + tokens[1].toString() + " " + tokens[2].toString());
							tokens[2] = tokens[2].replaceAll("\\.","");
							start = LocalDateTime.parse(tokens[1].toString(), dateTimeFormatter);
							Comment comment = new Comment(tokens[2], start);
							
							for(User tmpUser : users)
							{
								//System.out.println("-" + tokens[0] + "-");
								if(tmpUser.getEmail().toString().equals(tokens[0]))
								{
									comment.setUser(tmpUser);
									tmpUser.addComment(comment);
								}
							}
							String tmpEventId;
							for(Event tmpEvent : events)
							{
								tmpEventId = tmpEvent.getTitle() + tmpEvent.getCity() + tmpEvent.getContent();
								//System.out.println(eventStringId + " "+eventStringId.length()+ "----"+tmpEventId.length()+" " + tmpEventId);
								
								if(eventStringId.equalsIgnoreCase(tmpEventId))
								{
									//System.out.println(eventId + " " + tmpEvent.getId());
									comment.setEvent(tmpEvent);
									tmpEvent.addComment(comment);
								}
							}
							comments.add(comment);
							
						}
						else
						{
							//System.out.println("Event: " + tmp);
							String[] tokens = tmp.split("[,]");
							for(int tokPos =0; tokPos < tokens.length; tokPos++)
							{
								if(Character.isWhitespace(tokens[tokPos].charAt(0)))
								{
								tokens[tokPos] = tokens[tokPos].replaceFirst("\\s", "");
								}
								
								tokens[tokPos] = tokens[tokPos].replaceAll("\\[", "").replaceAll("\\]","").replaceAll("\\(","").replaceAll("\\)","");
								tokens[tokPos] = tokens[tokPos].trim();
								//System.out.println(tokens[tokPos]);
							}
							
							String[] dateTokens = tokens[3].split("[-]");
							
							for(int dateToks =0; dateToks < dateTokens.length; dateToks++ )
							{
								dateTokens[dateToks] = dateTokens[dateToks].trim();
							}
							
							start = LocalDateTime.parse(dateTokens[0].toString(), dateTimeFormatter);
							stop = LocalDateTime.parse(dateTokens[0].toString(), dateTimeFormatter);
							
							Event event = new Event(tokens[0].toString(), tokens[1].toString(), tokens[2].toString(), start, stop);
							
							//System.out.println(tokens[0].toString() + " "+ tokens[1].toString());
							
							for(int i = 4; i< tokens.length; i++)
							{
								
								//System.out.println(tokens[i]);
								
								Organizer organizer = new Organizer();
								organizer.setEvent(event);
								
								for(User tmpUser: users)
								{
									if(tmpUser.getEmail().equals(tokens[i].toString()))
									{
										//System.out.println(tmpUser.getEmail());
										organizer.setUser(tmpUser);
										tmpUser.addOrganizer(organizer);
									}
								}
								event.addOrganizers(organizer);
								
							}
							eventStringId = event.getTitle() + event.getCity() + event.getContent();
							events.add(event);
							
						}
					}
				}
			}
		}
		catch(IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Set<User> getUsers()
	{
		return users;
	}

	public Set<Event> getEvents()
	{
		return events;
	}

	public Set<Comment> getComments()
	{
		return comments;
	}

	public Set<Organizer> getOrganizers()
	{
		return organizers;
	}
	
	public void testPrint() {
		for(Event tmpEvent : events)
		{	
			System.out.println(tmpEvent.getTitle() + " " + tmpEvent.getCity() + " " + tmpEvent.getContent() + " " + tmpEvent.getStartTime() + " "+ tmpEvent.getEndTime());
			for(Comment tmpComment : tmpEvent.getComments())
			{
				System.out.println("        "+tmpComment.getUser().getEmail() + " " + tmpComment.getTime() + " " + tmpComment.getComment());
			}
			
		}
	}

}
