
package se.miun.dsv.jee.jpa.example;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import se.miun.dsv.jee.jpa.example.model.Event;
import se.miun.dsv.jee.jpa.example.model.User;

public class DatabaseService {
	
	DAO dao;
	EventsParser ep;
	
	public DatabaseService() {
		dao = new DAO();
		ep =  new EventsParser();
	}
	
	public void populate() {
		ep.parse();		
		Set<User> users = new HashSet<>();
		users = ep.getUsers();
		dao.persistAll(users);
	}
	
	public void addEvent(Event event) {
		dao.persistEvent(event);
	}
	
	public List<Event> getEventByCity(String city) {
		return dao.queryEventsByCity(city);
	}
	
	public List<Event> getAllEvents() {
		return dao.queryAllEvents();
	}
	
	public Event getEventById(long id) {
		return dao.queryEventById(id);
	}
	
	public User getUserById(long id)
	{
		return dao.queryUserById(id);
	}
	
	public List<User> getAllUsers() {
		return dao.queryAllUsers();
	}
	
	public void shutDown() {
		dao.shutDown();
	}
	
	/*public static void main(String[] args) {
	DatabaseService ds = new DatabaseService();
	ds.populate();
	ds.shutDown();
	}
		*/
}
