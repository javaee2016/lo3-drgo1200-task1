

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import se.miun.dsv.jee.jpa.example.DatabaseService;

/**
 * Application Lifecycle Listener implementation class ContextListener
 *
 */
@WebListener
public class ContextListener implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public ContextListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0)  { 

    	
    	//shutdown the database service
    	DatabaseService ds = (DatabaseService) arg0.getServletContext().getAttribute("db");
    	ds.shutDown();
    	System.out.println("Servlet Context Listener destroyed");
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg0)  { 
    	
    	//create a database service
    	DatabaseService ds = new DatabaseService();
    	ds.populate();
    	
    	//add it to the servlet context
    	ServletContext sc = arg0.getServletContext();
    	sc.setAttribute("db", ds);

    	System.out.println("Servlet Context Listener initialized");
    }
	
}
