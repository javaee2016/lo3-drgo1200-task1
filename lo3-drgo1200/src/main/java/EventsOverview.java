

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.miun.dsv.jee.jpa.example.DatabaseService;
import se.miun.dsv.jee.jpa.example.model.Event;

/**
 * Servlet implementation class EventsOverview
 */
@WebServlet("/Events-Overview")
public class EventsOverview extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	ThreadSafeCounter counter = new ThreadSafeCounter();
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		counter.increment(); //counter
		
		//create databaseservice
		DatabaseService ds = (DatabaseService)getServletContext().getAttribute("db");
		request.setCharacterEncoding("UTF-8");
		
		if(request.getParameter("city") == null) { //if no search list all events
			List<Event> events = ds.getAllEvents();
			request.setAttribute("events", events);
		}
		else { //else list events based on city name
			List<Event> events = ds.getEventByCity(request.getParameter("city"));
			request.setAttribute("events", events);
		}
		
		request.setAttribute("counter", counter.value()); //add counter
		
		
		request.getRequestDispatcher("/EventsOverview.jsp").forward(request, response);
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
