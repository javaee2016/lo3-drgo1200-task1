

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.miun.dsv.jee.jpa.example.DatabaseService;
import se.miun.dsv.jee.jpa.example.model.Event;
import se.miun.dsv.jee.jpa.example.model.Organizer;
import se.miun.dsv.jee.jpa.example.model.User;

/**
 * Servlet implementation class EventView
 */
@WebServlet("/Event-View")
public class EventView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	ThreadSafeCounter counter = new ThreadSafeCounter();
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		counter.increment(); //increment counter
		
		//create database service
		DatabaseService ds = (DatabaseService)getServletContext().getAttribute("db");
		
		Long tmpId = Long.parseLong((String) request.getParameter("eventAttr")); //id of event to check

		Event event = ds.getEventById(tmpId); //generate event from ds
		
		request.setAttribute("event", event); //add the event to request
		
		List<User> users = new ArrayList<User>();
		
		for(Organizer org : event.getOrganizers()) //add all the users from the event to a list
		{
			users.add(org.getUser());
		}
		
		//set all event info into attributes
		request.setAttribute("users", users);
		request.setAttribute("comments", event.getComments());
		
		request.setAttribute("counter", counter.value());
		
		request.getRequestDispatcher("/EventView.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
