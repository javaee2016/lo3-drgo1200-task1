<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.time.LocalDateTime"%>
<%@ page import="se.miun.dsv.jee.jpa.example.model.Event"%>
<%@ page import="se.miun.dsv.jee.jpa.example.model.Comment"%>
<%@ page import="java.time.format.DateTimeFormatter"%>
<%@ page import="java.util.Set"%>

<!DOCTYPE html>
<html>
<head> 
<meta charset="UTF-8">
<title>Event</title>
</head>
<body>
<a href="index.jsp">Home</a>
<p>Requests: ${counter}</p>
<br></br>
	<p>----------Info</p>
	<p>Title: ${event.title}</p>
	<p>City: ${event.city}</p>
	<p>Description ${event.content}</p>

	<p>Starting:</p>
	<%
		
			Event tmpEvent = (Event) request.getAttribute("event");
			LocalDateTime startTime = tmpEvent.getStartTime();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yy/MM/dd HH:mm");
			String formattedTime = startTime.format(formatter);
			out.println("<p>"+formattedTime+"</p>");
	%>
	<p>Ending:</p>
	<%
			
			LocalDateTime endTime = tmpEvent.getEndTime();
			formattedTime = endTime.format(formatter);
			out.println("<p>"+formattedTime+"</p>");
		
	%>

	<p>----------Users:</p>

	<c:forEach items="${users}" var="user">

		<p>
			<a href="User-View?userAttr=${user.id}">
			<c:out value="${user.firstName}" /> 
			<c:out value="${user.lastName}" />
			<c:out value="${user.email}" />
</a>


			<br></br>
		</p>
	</c:forEach>

	<%
			Set<Comment> tmpComments = tmpEvent.getComments();
			out.println("----------Comments:<br></br>");

			for (Comment comment : tmpComments) {
				String formattedCommentTime = comment.getTime().format(formatter);
				out.println(formattedCommentTime + " " + comment.getUser().getFirstName() + " "
						+ comment.getUser().getLastName() + " " + comment.getUser().getEmail() + ":<br></br>");
				out.println(comment.getComment() + "<br></br>");
			}
		
	%>


</body>
</html>